import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GyuudonGUI {
    int currentPrice = 0;
    void order(String food,int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + '?',
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null,"Order for " + food + " received.");

            String currentText = textArea1.getText();
            textArea1.setText(currentText + food + " " + price + " yen" + "\n");

            currentPrice += price;
            TotalPrice.setText(String.valueOf(currentPrice));

        }
    }
    private JButton gyuDonButton;
    private JButton gyukarubiButton;
    private JButton karaagedonButton;
    private JTextArea textArea1;
    private JButton Checkout;
    private JButton negitamaButton;
    private JButton unagiButton;
    private JButton eggButton;
    private JLabel Total;
    private JLabel TotalPrice;
    private JPanel root;
    private JLabel yen;

    public GyuudonGUI() {
        gyuDonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("gyudon", 450);
            }
        });
        gyukarubiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("gyukarubidon",630);
            }
        });
        karaagedonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("karaagedon",530);
            }
        });
        negitamaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("negitamagyudon",600);

            }
        });
        unagiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("unazyu",1200);
            }
        });
        eggButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("egg",100);
            }
        });
        Checkout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you for ordering!!");
                    textArea1.setText("");
                    TotalPrice.setText("0");
                }
            }
        });
    }




    public static void main(String[] args) {
        JFrame frame = new JFrame("GyuudonGUI");
        frame.setContentPane(new GyuudonGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}